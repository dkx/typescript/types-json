# DKX/TypesJson

Types definitions for json.

## Installation

```bash
$ npm install --save-dev @dkx/types-json
```
