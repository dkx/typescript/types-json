export declare type JsonScalar = string|number|boolean|null;
export declare type JsonValue = JsonScalar|JsonArray|JsonObject;
export declare type JsonObject = {[key: string]: JsonValue};
export declare interface JsonArray extends Array<JsonValue> {}
